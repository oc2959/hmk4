""" This code contains all of the main events of pong GUI"""

import time
import numpy as np

def dot(x, y):
	"""
	2D dot product
	"""
	return x[0]*y[0] + x[1]*y[1]

def scale(x, a):
	"""
	2D scalar multiplication
	"""
	return (x[0]*a, x[1]*a)

def add(x, y):
	"""
	2D vector addition
	"""
	return (x[0]+y[0], x[1]+y[1])

class Ball(object):
	def __init__(self, _canvas,_ball):
		"""
		Create a pong ball with the given inital position. 
		"""
		self.vel = np.linspace(1,10,1000)
		self.balldiameter = _canvas.coords(_ball)[2] - _canvas.coords(_ball)[0]
		self.position = (_canvas.coords(_ball)[0] + self.balldiameter/2.0,
		_canvas.coords(_ball)[1] + self.balldiameter/2.0)
		self.velocity = (10, 10)
		self.canvas_ = _canvas
		self.ball_ = _ball
		self.Nhits = 0

	def reflect(self, surface,opt):
		"""
		Alter the ball's velocity for a perfectly elastic
		collision with a surface defined by the unit normal surface.
		
		surface: a tuple of floats
		"""
		diagonal = -2 * dot(surface, self.velocity)
		self.velocity = add(self.velocity, scale(surface, diagonal))

		if (opt == 'paddle'):
			
			self.Nhits = self.Nhits + 1

	def move(self):
		"""
		Increment ball position, assuming no collisions.
		"""
		if (self.Nhits % 5 == 0 and self.Nhits != 0):
			
			self.velocity = scale (self.velocity,self.vel[self.Nhits/5 - 1])

		self.position = add(self.position, self.velocity)
		time.sleep(0.025)
		self.canvas_.move(self.ball_,self.velocity[0], self.velocity[1])
		self.canvas_.update()

	def reset(self):
		"""Reset the position, velocity and number of hits of the ball"""

		self.position = [float(self.canvas_['width'])/2.0, float(self.canvas_['height'])/2.0]
		self.canvas_.coords(self.ball_,self.position[0],self.position[1],
			self.position[0] + self.balldiameter,self.position[1] + self.balldiameter)
		self.Nhits = 0
		self.velocity = (10, 10)

class Paddle(object):
	def __init__(self,root,_canvas,_paddle):
		"""
		Create a paddle with the given inital position. 
		Assign methods to it
		Bind Keys
		"""
		self.position_ymin = [0,0] 
		self.position_ymax = [0,0] 
		self.position_xout = [0,0]

		self.paddle_ = _paddle
		self.canvas_ = _canvas
		self.direction_right = 0
		self.direction_left = 0

		def upright(event):
			self.direction_right = -15

		def downright(event):
			self.direction_right = +15

		def upleft(event):
			self.direction_left = -15

		def downleft(event):
			self.direction_left = +15

		def onkeyrelease(event):
			key = event.keysym
			
			if (str(key)=="Up" or str(key)=="Down"):
				self.direction_right = 0

			if (str(key)=="w" or str(key)=="s"):
				self.direction_left = 0

		root.bind("<Up>",upright)
		root.bind("<Down>",downright)
		root.bind("<w>",upleft)
		root.bind("<s>",downleft)
		root.bind('<KeyRelease>', onkeyrelease)

	def left_paddle_constraint(self):

		"""prevent left paddle from moving out of screen"""

		return not ((self.canvas_.coords(self.paddle_[0])[1] < 0 
			and self.direction_left<0) 
			or (self.canvas_.coords(self.paddle_[0])[3] > float(self.canvas_['height']) 
			and self.direction_left>0))

	def right_paddle_constraint(self):

		"""prevent right paddle from moving out of screen"""

		return not ((self.canvas_.coords(self.paddle_[1])[1] < 0 
			and self.direction_right<0)
			or (self.canvas_.coords(self.paddle_[1])[3] > float(self.canvas_['height'])
			and self.direction_right>0))

	def move(self):

		"""move paddle"""

		if (self.left_paddle_constraint()):
			self.canvas_.move(self.paddle_[0],0, self.direction_left)

		if (self.right_paddle_constraint()):
			self.canvas_.move(self.paddle_[1],0, self.direction_right)

		self.position_ymin[0] = self.canvas_.coords(self.paddle_[0])[1]
		self.position_ymax[0] = self.canvas_.coords(self.paddle_[0])[3]
		self.position_xout[0] = self.canvas_.coords(self.paddle_[0])[2]

		self.position_ymin[1] = self.canvas_.coords(self.paddle_[1])[1]
		self.position_ymax[1] = self.canvas_.coords(self.paddle_[1])[3]
		self.position_xout[1] = self.canvas_.coords(self.paddle_[1])[0]


class Score(object):
	def __init__(self,score_):
		"""
		Track the score
		"""
		self.score = score_

	def addPoint(self,player):
		self.score[player].set(int(self.score[player].get())+1)

	def reset(self):
		self.score[0].set(0)
		self.score[1].set(0)
		
		
class Pong(object):

	"""main routine"""
	"""create instances of the other class and create callbacks"""

	def __init__(self,root,_canvas,_ball,_paddle,_score):

		self.ballc = Ball(_canvas,_ball)
		self.paddlec = Paddle( root,_canvas,_paddle)
		self.scorec = Score(_score)

		self.cheight = float(_canvas['height'])
		self.cwidth = float(_canvas['width'])

		self.do = 1

		# either of these events could be defined with a lambda, but I 
		# wanted to show you how to refer to methods without calling them
		self.events = [self.hits_upper_wall,
					   self.hits_lower_wall,
					   self.hits_right_paddle,
					   self.hits_left_paddle,
					   self.right_win, 
					   self.left_win,
					   self.right_win, 
					   self.left_win]
		# each event function has a corresponding response function
		# a response function is a callback function
		self.responses = [lambda: self.ballc.reflect((0,1),'wall'),
						lambda: self.ballc.reflect((0,-1),'wall'),
						lambda: self.ballc.reflect((-1,0),'paddle'), 
						lambda: self.ballc.reflect((1,0),'paddle'),
						lambda: self.scorec.addPoint(1), 
						lambda: self.scorec.addPoint(0),
						self.ballc.reset, 
						self.ballc.reset]

	def hits_upper_wall(self):
		return self.ballc.position[1] < self.ballc.balldiameter/2.0

	def hits_lower_wall(self):
		return self.ballc.position[1] > self.cheight - self.ballc.balldiameter/2.0

	def hits_left_paddle(self):

		ballx = self.ballc.position[0] - self.ballc.balldiameter/2.0 
		ballymax = self.ballc.position[1] + self.ballc.balldiameter/2.0
		ballymin = self.ballc.position[1] - self.ballc.balldiameter/2.0

		return  (ballymax>self.paddlec.position_ymin[0] 
			and ballymin<self.paddlec.position_ymax[0] 
			and ballx < self.paddlec.position_xout[0])

	def hits_right_paddle(self):
		
		ballx = self.ballc.position[0] + self.ballc.balldiameter/2.0 
		ballymax = self.ballc.position[1] + self.ballc.balldiameter/2.0
		ballymin = self.ballc.position[1] - self.ballc.balldiameter/2.0

		return  (ballymax> self.paddlec.position_ymin[1] 
			and ballymin<self.paddlec.position_ymax[1] 
			and ballx > self.paddlec.position_xout[1])

	def left_win(self):
		
		ballx = self.ballc.position[0] + self.ballc.balldiameter/2.0
		ballymax = self.ballc.position[1] + self.ballc.balldiameter/2.0
		ballymin = self.ballc.position[1] - self.ballc.balldiameter/2.0

		return  ((ballymax< self.paddlec.position_ymin[1] 
			or ballymin>self.paddlec.position_ymax[1])
			and ballx > self.cwidth)

	def right_win(self):
		
		ballx = self.ballc.position[0] - self.ballc.balldiameter/2.0
		ballymax = self.ballc.position[1] + self.ballc.balldiameter/2.0
		ballymin = self.ballc.position[1] - self.ballc.balldiameter/2.0

		return  ((ballymax<self.paddlec.position_ymin[0] 
			or ballymin>self.paddlec.position_ymax[0])
			and ballx < 0)

	def step(self):
		"""
		Calculate the next game state.
		"""
		self.ballc.move()
		self.paddlec.move()
		# check for events
		for event, response in zip(self.events, self.responses):
			if event():
				response()


	def run(self):
		"""
		For the game loop.
		""" 
		while self.do:
			self.step()

			
