
import Tkinter as Tk
import pongevents as p_e
import time

"""this code sets up the basic frame of the code including
the  canvas, ball, paddle, scores and buttons"""

class Application(Tk.Frame):

	def __init__(self, master=None):

		Tk.Frame.__init__(self, master)
		self.grid()

		self.master.title('Pong')

		# inputs

		canvaswidth = 800
		canvasheight = 600
		ballDiameter = 20
		playerheight = 100
		playerwidth = 25
	
		# subdivide into 21 rows and 6 columns

		for r in range(21):
			self.master.rowconfigure(r, weight=1)    
		for c in range(12):
			self.master.columnconfigure(c, weight=1)

		# start and quit buttons
		
		self.restartBB = Tk.Button(master, text="Restart",command=self.restart, bg="white").grid(row=21,column=0,columnspan = 6,sticky=Tk.E+Tk.W)
		self.quitBB = Tk.Button(master, text="Quit",command=self.quit, bg="white").grid(row=21,column=6,columnspan = 6,sticky=Tk.E+Tk.W)
		
		# the frame containing the points

		self.Frame = []

		self.Frame.append(Tk.Frame(master,width = 60, height = canvasheight))
		self.Frame.append(Tk.Frame(master,width = 60, height = canvasheight))
	
		self.Frame[0].grid(row = 0, column = 0, rowspan = 21, columnspan = 1, sticky = Tk.W+Tk.E+Tk.N+Tk.S)
		self.Frame[1].grid(row = 0, column = 11, rowspan = 21, columnspan = 1, sticky = Tk.W+Tk.E+Tk.N+Tk.S)
		self.Frame[0].pack_propagate(0)
		self.Frame[1].pack_propagate(0)

		# the left player score keeping variable

		scoreleft = Tk.StringVar()
		label = Tk.Label( self.Frame[0], textvariable = scoreleft, relief = Tk.RAISED, bg = "blue")
		label.configure(fg='white',font=("Helvetica", 20))
		label.pack(side=Tk.TOP, fill=Tk.BOTH, expand=1)

		# the right player score keeping variable

		scoreright = Tk.StringVar()
		label = Tk.Label( self.Frame[1], textvariable = scoreright, relief = Tk.RAISED, bg = "red")
		label.configure(fg='white',font=("Helvetica", 20))
		label.pack(side=Tk.TOP, fill=Tk.BOTH, expand=1)

		# main canvas holding the animation of the ball and paddles

		canvas = Tk.Canvas(master, bg = "black",width = canvaswidth, height = canvasheight)
		canvas.grid(row = 0, column = 1, rowspan = 21, columnspan = 10, sticky = Tk.W+Tk.E+Tk.N+Tk.S)

		# set value of scores

		scoreleft.set(str(0))
		scoreright.set(str(0))

		# ball coordinates

		xball = (canvaswidth - ballDiameter)/2.0
		yball = (canvasheight - ballDiameter)/2.0

		# tkinter oval

		ball = canvas.create_oval(xball, yball, xball + ballDiameter,
			yball + ballDiameter,fill='white')

		# player coordinates

		xplayer2 = canvaswidth - playerwidth
		yplayer = (canvasheight - playerheight )/2.0

		# tkinter rectangle will be assigned to players

		playerone = canvas.create_rectangle(0,yplayer,playerwidth,yplayer + playerheight,fill='blue')
		playertwo = canvas.create_rectangle( xplayer2 ,yplayer,xplayer2 + playerwidth,
			yplayer + playerheight,fill='red')

		self.pong = p_e.Pong(master,canvas,ball,[playerone,playertwo],[scoreleft,scoreright])
		self.pong.run()

	def restart(self):
		Application(master=root) # restart application

	def quit(self):
		self.pong.do = 0 # stop animation
		self.master.destroy() # close window

root = Tk.Tk()
root.resizable(0,0)

app = Application(master=root)
root.mainloop()
